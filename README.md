# Bink Tech Test
## James O'Toole
07568396227 
contact@james-o.tools

Run the main program. The CSV is included in the repo for convinience, it will be used by default.
```
usage: main.py [-h] [--filepath FILEPATH] {first,second,third,fourth} ...

Bink Tech Test. Choose a requirement: first; second; third or fourth.

positional arguments:
  {first,second,third,fourth}
    first               Top 5 highest current rent
    second              List and total masts with 25 years lease
    third               Count masts per tenant
    fourth              List lease start dates between 1st June 1999 and 31st
                        August 2007

optional arguments:
  -h, --help            show this help message and exit
  --filepath FILEPATH   Path to CSV file
```

Run the unit tests
```
python test.py
```
