#!/usr/bin/python3

import csv
import argparse
from functools import reduce
from collections import Counter
from datetime import datetime


class MastData:
    def __init__(self, rows):
        self.rows = rows

    @staticmethod
    def rows_to_string(rows):
        rows = [', '.join(row) for row in rows]
        return '\n'.join(rows)

    @staticmethod
    def dict_to_string(d):
        # Sort by key so output is deterministic (for unit tests)
        sort_d = sorted(d.items(), key=lambda o: o[0])
        stringy_d = [': '.join((k, str(v))) for k, v in sort_d]
        return '\n'.join(stringy_d)

    def top_five(self):
        """
        Produce a list sorted by "Current Rent" in ascending order
        Obtain the first 5 items from the resultant list and output to the
        console
        """
        sorted_rows = sorted(self.rows, key=lambda row: float(row[10]))
        top_5 = sorted_rows[0:5]
        return MastData.rows_to_string(top_5)

    def list_and_total(self):
        """
        From the list of all mast data, create a new list of mast data with
        "Lease Years" = 25 years.
        Output the list to the console, including all data fields
        Output the total rent for all items in this list to the console
        """
        filtered_rows = [row for row in self.rows if row[9] == '25']
        total = reduce(lambda a, b: a + float(b[10]), filtered_rows, 0.0)
        return '{}\n\nTotal: {}'.format(
                MastData.rows_to_string(filtered_rows),
                total)

    def count_per_tenant(self):
        """
        Create a dictionary containing tenant name and a count of masts for
        each tenant
        Output the dictionary to the console in a readable form
        """
        counter = Counter()
        for row in self.rows:
            counter[row[6]] += 1
        return MastData.dict_to_string(counter)

    def filter_by_date(self):
        """
        List the data for rentals with "Lease Start Date" between
        1st June 1999 and 31st August 2007
        Output the data to the console with dates formatted as DD/MM/YYYY
        """
        rows = []
        for row in self.rows:
            date_string = row[7]
            dt = datetime.strptime(date_string, '%d %b %Y')
            if dt > datetime(day=1, month=6, year=1999) and \
                    dt < datetime(day=31, month=8, year=2007):
                _row = row.copy()
                _row[7] = dt.strftime('%d/%m/%Y')
                rows.append(_row)
        return MastData.rows_to_string(rows)


def main():
    parser = argparse.ArgumentParser(description=(
        'Bink Tech Test. Choose a requirement: '
        'first; second; third or fourth.'))
    parser.add_argument(
            '--filepath', dest='filepath', help='Path to CSV file',
            default='./dataset.csv')
    sub_parser = parser.add_subparsers(dest='command')
    sub_parser.required = True
    sub_parser.add_parser('first', help='Top 5 highest current rent')
    sub_parser.add_parser(
            'second', help='List and total masts with 25 years lease')
    sub_parser.add_parser('third',  help='Count masts per tenant')
    sub_parser.add_parser(
            'fourth', help=(
                'List lease start dates between '
                '1st June 1999 and 31st August 2007'))
    args = parser.parse_args()

    with open(args.filepath) as f:
        rows = csv.reader(f)
        next(rows)
        md = MastData(rows)
        if args.command == 'first':
            out = md.top_five()
        elif args.command == 'second':
            out = md.list_and_total()
        elif args.command == 'third':
            out = md.count_per_tenant()
        elif args.command == 'fourth':
            out = md.filter_by_date()
        print(out)


if __name__ == '__main__':
    main()
